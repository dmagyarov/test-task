# Create User

``
POST /api/users
``

Creates user with given credentials

##### Headers:

> Content-Type: application/json
> 
> Accept: application/json

##### Payload:

```
{
    "email": "test@example.com",
    "first_name": "Test",
    "password": "password123",
    "middle_name": "Test",
    "last_name": "Test",
    "contact_no": "+1234567890"
}
```

- email - valid email
- password - password, min length - 10
- first_name - user's first name
- middle_name - user's middle name
- last_name - user's last name
- contact_no - e164 phone number


##### Response codes: 

>200 - Success response

>422 - Request payload contains invalid data

>500 - Server error

##### Response example:
```
{
    "data": {
        "id": 3,
        "email": "aaedefr2wq@b.c",
        "first_name": "vasya",
        "middle_name": "pupkine",
        "last_name": "test",
        "contact_no": "+14334533738920"
    }
}
```
