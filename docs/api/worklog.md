# Get Work Log Index

``
GET /api/worklog
``

Fetches worklog entry index

##### Security
> Basic auth

##### Headers:

> Content-Type: application/json
>
> Accept: application/json

##### Request parameters:
- sort_by - *string*, enables sort by following fields: date, hours_worked, hours_late, hours_undertime, hours_overtime
- order_by - *string*, possible values: asc, desc
- user_id - *integer*, adds filtering by user id from database.

##### Responses:

> 200 - Success response

> 401 - Unauthorized

> 422 - Request payload contains invalid data

> 500 - Server error

##### Response Example

```
{
    "data": {
        "id": 14,
        "date": "2024-05-18",
        "time_in": "09:00 am",
        "time_out": "04:00 pm",
        "hours_worked": "06:00",
        "hours_late": "00:00",
        "hours_overtime": "00:00",
        "hours_undertime": "02:00",
        "user": {
            "id": 1,
            "email": "test@example.com",
            "first_name": "test",
            "middle_name": "test1",
            "last_name": "test2",
            "contact_no": "+143345378920"
        }
    }
}
```

* Resource response also contains pagination links

# Create Work Log entry

``
POST /api/worklog
``

Creates worklog entry with given details

##### Security
> Basic auth

##### Headers:

> Content-Type: application/json
>
> Accept: application/json

##### Body:

```
{
    "user_id": 1,
    "date": "2024-05-18",
    "time_in": "09:00 am",
    "time_out": "04:00 pm"
}
```

##### Responses:

> 200 - Success response

> 401 - Unauthorized

> 422 - Request payload contains invalid data

> 500 - Server error

- user_id - user id to whom log entry belongs
- date - date in Y-m-d format
- time_in - time when users started to work in "h:i a" format
- time_out - time when user finished in "h:i a" format

##### Response Example

```
{
    "data": {
        "id": 14,
        "date": "2024-05-18",
        "time_in": "09:00 am",
        "time_out": "04:00 pm",
        "hours_worked": "06:00",
        "hours_late": "00:00",
        "hours_overtime": "00:00",
        "hours_undertime": "02:00",
        "user": {
            "id": 1,
            "email": "aaedwq@b.c",
            "first_name": "vasya",
            "middle_name": "pupkine",
            "last_name": "test",
            "contact_no": "+143345378920"
        }
    }
}
```
