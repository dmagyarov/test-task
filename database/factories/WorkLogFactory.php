<?php

namespace Database\Factories;

use App\Calculators\WorkTimeCalculator;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\WorkLog>
 */
class WorkLogFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $timeIn = Carbon::createFromTimeString(fake()->time('H:i'));
        $timeOut = Carbon::createFromTimeString(fake()->time('H:i'));
        $hoursCalculator = new WorkTimeCalculator($timeIn, $timeOut);

        return [
            ...[
                'user_id' => rand(10, 100),
                'date' => date('Y-m-' . rand(1, 30)),
                'time_in' => $timeIn,
                'time_out' => $timeOut,
            ],
            ...[
                'hours_worked' => $hoursCalculator->workHours(),
                'hours_late' => $hoursCalculator->lateHours(),
                'hours_undertime' => $hoursCalculator->undertimeHours(),
                'hours_overtime' => $hoursCalculator->overtimeHours()
            ]
        ];
    }
}
