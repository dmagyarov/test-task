<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkLogController;

Route::post('/users', [UserController::class, 'store']);

Route::middleware('auth.basic')
    ->prefix('/logs')
    ->group(function () {
        Route::post('/', [WorkLogController::class, 'store']);
        Route::get('/', [WorkLogController::class, 'index']);
    });
