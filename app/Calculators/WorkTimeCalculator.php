<?php

namespace App\Calculators;

use Carbon\CarbonInterval;
use Illuminate\Support\Carbon;

final class WorkTimeCalculator
{
    const BREAK_HOUR_DURATION = 1;
    const HALF_WORK_DAY_HOURS = 4;
    const REGULAR_WORK_DAY_HOURS = 8;
    const WORK_DAY_START_HOUR = 9;

    /**
     * @var CarbonInterval
     */
    private readonly CarbonInterval $userWorkHours;

    /**
     * @var CarbonInterval
     */
    private readonly CarbonInterval $regularWorkTimeInterval;

    /**
     * @var Carbon
     */
    private readonly Carbon $workDayStartTime;

    /**
     * @param Carbon $timeIn
     * @param Carbon $timeOut
     */
    public function __construct(
        private readonly Carbon $timeIn,
        private readonly Carbon $timeOut
    )
    {
        $this->userWorkHours = $this->timeIn->diffAsCarbonInterval($this->timeOut);
        $this->regularWorkTimeInterval = CarbonInterval::hours(self::REGULAR_WORK_DAY_HOURS);
        $this->workDayStartTime = Carbon::createFromTime(self::WORK_DAY_START_HOUR);
    }

    /**
     * @return CarbonInterval
     */
    public function breakTime(): CarbonInterval
    {
        if ($this->userWorkHours->hours < self::HALF_WORK_DAY_HOURS) {
            return $this->zeroInterval();
        }

        return CarbonInterval::hours(self::BREAK_HOUR_DURATION);
    }

    /**
     * @return CarbonInterval
     */
    public function workHours(): CarbonInterval
    {
        return (clone $this->userWorkHours)->sub($this->breakTime());
    }

    /**
     * @return CarbonInterval
     */
    public function lateHours(): CarbonInterval
    {
        if ($this->timeIn->gt($this->workDayStartTime)) {
            return $this->workDayStartTime->diffAsCarbonInterval($this->timeIn);
        }

        return $this->zeroInterval();
    }

    /**
     * @return CarbonInterval
     */
    public function overtimeHours(): CarbonInterval
    {
        return !$this->workDurationDifference()->invert
            ? $this->workDurationDifference()
            : $this->zeroInterval();
    }

    /**
     * @return CarbonInterval
     */
    public function undertimeHours(): CarbonInterval
    {
        return $this->workDurationDifference()->invert
            ? $this->workDurationDifference()->invert()
            : $this->zeroInterval();
    }

    /**
     * @return CarbonInterval
     */
    public function zeroInterval(): CarbonInterval
    {
        return CarbonInterval::seconds(0);
    }

    /**
     * @return CarbonInterval
     */
    private function workDurationDifference(): CarbonInterval
    {
        return $this->workHours()->sub($this->regularWorkTimeInterval);
    }
}
