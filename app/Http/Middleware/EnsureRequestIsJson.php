<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

class EnsureRequestIsJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!$request->wantsJson()) {
            throw new UnsupportedMediaTypeHttpException(
                __('Invalid Accept header. Only application/json is allowed.')
            );
        }

        if ($request->header('Content-Type') !== 'application/json') {
            throw new UnsupportedMediaTypeHttpException(
                'Invalid Content-Type. Only application/json is allowed.'
            );
        }

        return $next($request);
    }
}
