<?php

namespace App\Http\Controllers;

use App\Actions\CreateUserAction;
use App\Http\Requests\CreateUserRequest;
use App\Http\Resources\UserResource;

final class UserController extends Controller
{
    /**
     * @param CreateUserAction $createUserAction
     * @param CreateUserRequest $request
     * @return UserResource
     */
    public function store(
        CreateUserAction $createUserAction,
        CreateUserRequest $request
    ): UserResource {
        return new UserResource($createUserAction->handle($request->toDTO()));
    }
}
