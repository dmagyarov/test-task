<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWorkLogRequest;
use App\Http\Requests\WorkLogIndexRequest;
use App\Http\Resources\WorkLogResource;
use App\Repositories\WorkLogRepository;
use App\Services\WorkLogService;
use Illuminate\Http\Resources\Json\ResourceCollection;

final class WorkLogController extends Controller
{
    /**
     * @param WorkLogRepository $workLogRepository
     * @param WorkLogIndexRequest $request
     * @return ResourceCollection
     */
    public function index(
        WorkLogRepository $workLogRepository,
        WorkLogIndexRequest $request
    ): ResourceCollection {
        return WorkLogResource::collection($workLogRepository->all($request->toDTO()));
    }

    /**
     * @param WorkLogService $workLogService
     * @param CreateWorkLogRequest $request
     * @return WorkLogResource
     */
    public function store(
        WorkLogService $workLogService,
        CreateWorkLogRequest $request
    ): WorkLogResource {
        return new WorkLogResource($workLogService->create($request->toDTO()));
    }
}
