<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

final class WorkLogResource extends JsonResource
{
    private const INTERVAL_FORMAT = '%H:%S';

    private const WORK_HOURS_FORMAT = 'h:i a';

    /**
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'date' => $this->date->format('Y-m-d'),
            'time_in' => $this->time_in->format(self::WORK_HOURS_FORMAT),
            'time_out' => $this->time_out->format(self::WORK_HOURS_FORMAT),
            'hours_worked' => $this->hours_worked->format(self::INTERVAL_FORMAT),
            'hours_late' => $this->hours_late->format(self::INTERVAL_FORMAT),
            'hours_overtime' => $this->hours_overtime->format(self::INTERVAL_FORMAT),
            'hours_undertime' => $this->hours_undertime->format(self::INTERVAL_FORMAT),
            'user' => new UserResource($this->user)
        ];
    }
}
