<?php

namespace App\Http\Requests;

use App\Contracts\DTO;
use App\Contracts\HasDTO;
use App\DTO\CreateWorkLogDTO;
use App\Repositories\WorkLogRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

final class CreateWorkLogRequest extends FormRequest implements HasDTO
{
    const TIME_FORMAT = 'h:i a';
    const DATE_FORMAT = 'Y-m-d';

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|numeric|exists:users,id',
            'date' => 'required|date_format:' . self::DATE_FORMAT,
            'time_in' => 'required|date_format:' . self::TIME_FORMAT,
            'time_out' => 'required|date_format:' . self::TIME_FORMAT,
        ];
    }

    /**
     * Method ensures that provided work hours were NOT already added for particular user
     *
     * @param WorkLogRepository $workLogRepository
     * @return Closure[]
     */
    public function after(WorkLogRepository $workLogRepository): array
    {
        return [
            function (Validator $validator) use ($workLogRepository) {
                if ($workLogRepository->countOverlappingHours($this->toDTO()) > 0) {
                    $validator->errors()->add(
                        'work_hours',
                        __('Given work hours are already added')
                    );
                }
            }
        ];
    }

    /**
     * @return CreateWorkLogDTO
     */
    public function toDTO(): DTO
    {
        return new CreateWorkLogDTO(
            $this->post('user_id'),
            Carbon::createFromFormat(self::DATE_FORMAT, $this->post('date')),
            Carbon::createFromTimeString($this->post('time_in')),
            Carbon::createFromTimeString($this->post('time_out'))
        );
    }

    /**
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'time_in' => Str::lower($this->post('time_in')),
            'time_out' => Str::lower($this->post('time_out')),
        ]);
    }
}
