<?php

namespace App\Http\Requests;

use App\Contracts\HasDTO;
use App\DTO\WorkLogIndexDTO;
use Illuminate\Foundation\Http\FormRequest;

final class WorkLogIndexRequest extends FormRequest implements HasDTO
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'sort_by' => 'in:date,hours_worked,hours_late,hours_undertime,hours_overtime',
            'order' => 'in:asc,desc',
            'user_id' => 'numeric|exists:users,id'
        ];
    }

    public function toDTO(): WorkLogIndexDTO
    {
        return new WorkLogIndexDTO(
            $this->get('sort_by'),
            $this->get('order'),
            $this->get('user_id')
        );
    }
}
