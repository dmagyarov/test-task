<?php

namespace App\Http\Requests;

use App\Contracts\HasDTO;
use App\DTO\CreateUserDTO;
use Illuminate\Foundation\Http\FormRequest;

final class CreateUserRequest extends FormRequest implements HasDTO
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $contactNumberRegex = '/^\+[1-9]\d{1,14}$/i'; // for numbers in e164 format (+XXXXXXXXXX)

        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:10',
            'first_name' => 'required|min:2|max:255',
            'middle_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'contact_no' => "required|regex:$contactNumberRegex|unique:users,contact_no"
        ];
    }

    /**
     * @return CreateUserDTO
     */
    public function toDTO(): CreateUserDTO
    {
        return new CreateUserDTO(
            $this->post('email'),
            $this->post('password'),
            $this->post('first_name'),
            $this->post('middle_name'),
            $this->post('last_name'),
            $this->post('contact_no')
        );
    }
}
