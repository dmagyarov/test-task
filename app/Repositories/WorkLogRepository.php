<?php

namespace App\Repositories;

use App\DTO\CreateWorkLogDTO;
use App\DTO\WorkLogIndexDTO;
use App\Http\Requests\WorkLogIndexRequest;
use App\Models\WorkLog;
use Illuminate\Pagination\LengthAwarePaginator;

final class WorkLogRepository
{
    /**
     * @param WorkLogIndexDTO $workLogIndexDTO
     * @return LengthAwarePaginator
     */
    public function all(WorkLogIndexDTO $workLogIndexDTO): LengthAwarePaginator
    {
        $query = WorkLog::with('user');

        if ($workLogIndexDTO->userId) {
            $query->where('user_id', $workLogIndexDTO->userId);
        }

        $query->orderBy($workLogIndexDTO->sort, $workLogIndexDTO->order);

        return $query->paginate();
    }

    /**
     * @param CreateWorkLogDTO $workLogDTO
     * @return WorkLog
     */
    public function create(CreateWorkLogDTO $workLogDTO, array $calculatedWorkHours): WorkLog
    {
        return WorkLog::create([
            ...[
                'user_id' => $workLogDTO->userId,
                'date' => $workLogDTO->date,
                'time_in' => $workLogDTO->timeIn,
                'time_out' => $workLogDTO->timeOut,
            ],
            ...$calculatedWorkHours
        ]);
    }

    /**
     * @param CreateWorkLogDTO $workLogDTO
     * @return int
     */
    public function countOverlappingHours(CreateWorkLogDTO $workLogDTO): int
    {
        return WorkLog::query()
            ->where('user_id', $workLogDTO->userId)
            ->where('date', $workLogDTO->date->format('Y-m-d'))
            ->where(
                fn($query) => $query
                    ->where('time_in', '<', $workLogDTO->timeOut)
                    ->where('time_out', '>', $workLogDTO->timeIn)
            )
            ->count();
    }
}
