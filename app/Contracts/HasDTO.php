<?php

namespace App\Contracts;

interface HasDTO
{
    /**
     * @return DTO
     */
    public function toDTO(): DTO;
}
