<?php

namespace App\Actions;

use App\DTO\CreateUserDTO;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

final class CreateUserAction
{
    public function handle(CreateUserDTO $userDTO): User
    {
        return User::create([
            'email' => $userDTO->email,
            'password' => Hash::make($userDTO->password),
            'first_name' => $userDTO->firstName,
            'middle_name' => $userDTO->middleName,
            'last_name' => $userDTO->lastName,
            'contact_no' => $userDTO->contactNo
        ]);
    }
}
