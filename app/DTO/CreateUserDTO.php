<?php

namespace App\DTO;

use App\Contracts\DTO;

final class CreateUserDTO implements DTO
{
    /**
     * @param string $email
     * @param string $firstName
     * @param string $middleName
     * @param string $password
     * @param string $lastName
     * @param string $contactNo
     */
    public function __construct(
        public readonly string $email,
        public readonly string $password,
        public readonly string $firstName,
        public readonly string $middleName,
        public readonly string $lastName,
        public readonly string $contactNo
    )
    {
    }
}
