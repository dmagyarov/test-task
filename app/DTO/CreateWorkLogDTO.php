<?php

namespace App\DTO;

use App\Contracts\DTO;
use Illuminate\Support\Carbon;

final class CreateWorkLogDTO implements DTO
{
    /**
     * @param int $userId
     * @param Carbon $date
     * @param Carbon $timeIn
     * @param Carbon $timeOut
     */
    public function __construct(
        public readonly int $userId,
        public readonly Carbon $date,
        public readonly Carbon $timeIn,
        public readonly Carbon $timeOut
    )
    {
    }
}
