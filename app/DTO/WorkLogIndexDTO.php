<?php

namespace App\DTO;

use App\Contracts\DTO;

final class WorkLogIndexDTO implements DTO
{
    const DEFAULT_SORT_ORDER = 'desc';
    const DEFAULT_SORT_FIELD = 'date';

    public function __construct(
        public readonly ?string $sort = self::DEFAULT_SORT_ORDER,
        public readonly ?string $order = self::DEFAULT_SORT_FIELD,
        public readonly ?int $userId = null
    )
    {
    }
}
