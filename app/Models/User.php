<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Authenticatable
{
    use HasFactory;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'password',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'contact_no',
    ];

    /**
     * @var string[]
     */
    protected $hidden = ['password'];

    /**
     * @return HasMany
     */
    public function workLogs(): HasMany
    {
        return $this->hasMany(WorkLog::class);
    }
}
