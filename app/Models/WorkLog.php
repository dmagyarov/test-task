<?php

namespace App\Models;

use App\Casts\TimeInterval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WorkLog extends Model
{
    use HasFactory;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $table = 'work_log';

    /**
     * @var string[]
     */
    protected $with = ['user'];

    /**
     * @var string[]
     */
    protected $casts = [
        'date' => 'date:Y-m-d',
        'time_in' => 'datetime:H:i:s',
        'time_out' => 'datetime:H:i:s',
        'hours_worked' => TimeInterval::class,
        'hours_late' => TimeInterval::class,
        'hours_undertime' => TimeInterval::class,
        'hours_overtime' => TimeInterval::class
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'date',
        'time_in',
        'time_out',
        'hours_worked',
        'hours_late',
        'hours_undertime',
        'hours_overtime'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
