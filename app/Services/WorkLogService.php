<?php

namespace App\Services;

use App\Calculators\WorkTimeCalculator;
use App\DTO\CreateWorkLogDTO;
use App\Models\WorkLog;
use App\Repositories\WorkLogRepository;

final class WorkLogService
{
    public function __construct(private readonly WorkLogRepository $workLogRepository)
    {
    }

    public function create(CreateWorkLogDTO $workLogDTO): WorkLog
    {
        $hoursCalculator = new WorkTimeCalculator($workLogDTO->timeIn, $workLogDTO->timeOut);

        return $this->workLogRepository->create($workLogDTO, [
            'hours_worked' => $hoursCalculator->workHours(),
            'hours_late' => $hoursCalculator->lateHours(),
            'hours_undertime' => $hoursCalculator->undertimeHours(),
            'hours_overtime' => $hoursCalculator->overtimeHours()
        ]);
    }
}
