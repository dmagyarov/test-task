### Test task

####  Installation instructions

- Git clone the repo
- Copy the .env file by running 
```cp .env.example .env```
- Execute the following in the cloned folder
```
docker run --rm \
  -u "$(id -u):$(id -g)" \
  -v $(pwd):/var/www/html \
  -w /var/www/html \
  laravelsail/php81-composer:latest \
  composer install --ignore-platform-reqs
```
- Run `php artisan sail:install` and choose mysql
- After that execute: ``sail up``
- Then run
``sail aritsan migrate``

Now the project is ready to work

Troubleshooting:
If there will be MySQL any access denied or connection refused errors, try to run the following command:
``sail down -v && sail up``

API docs located in /docs folder. 
