<?php

namespace Tests\Unit;

use App\Calculators\WorkTimeCalculator;
use Carbon\CarbonInterval;
use Illuminate\Support\Carbon;
use PHPUnit\Framework\TestCase;

class WorkTimeCalculatorTest extends TestCase
{
    public function test_without_break_hours(): void
    {
        $timeCalculator = new WorkTimeCalculator(
            Carbon::createFromTimeString('9:00am'),
            Carbon::createFromTimeString('11:00am'),
        );

        $this->assertEquals($timeCalculator->zeroInterval(), $timeCalculator->breakTime());
        $this->assertEquals(CarbonInterval::hour(0), $timeCalculator->lateHours());
        $this->assertEquals(CarbonInterval::hour(2), $timeCalculator->workHours());
        $this->assertEquals(CarbonInterval::hour(0), $timeCalculator->overtimeHours());
        $this->assertEquals(CarbonInterval::hour(6), $timeCalculator->undertimeHours());
    }

    public function test_with_break_hours(): void
    {
        $timeCalculator = new WorkTimeCalculator(
            Carbon::createFromTimeString('9:00am'),
            Carbon::createFromTimeString('6:00pm'),
        );

        $this->assertEquals(CarbonInterval::hour(1), $timeCalculator->breakTime());
        $this->assertEquals(CarbonInterval::hour(0), $timeCalculator->lateHours());
        $this->assertEquals(CarbonInterval::hour(8), $timeCalculator->workHours());
        $this->assertEquals(CarbonInterval::hour(0), $timeCalculator->overtimeHours());
        $this->assertEquals(CarbonInterval::hour(0), $timeCalculator->undertimeHours());
    }

    public function test_overtime_hours(): void
    {
        $timeCalculator = new WorkTimeCalculator(
            Carbon::createFromTimeString('9:00am'),
            Carbon::createFromTimeString('11:00pm'),
        );

        $this->assertEquals(CarbonInterval::hour(5), $timeCalculator->overtimeHours());
        $this->assertEquals(CarbonInterval::hour(0), $timeCalculator->undertimeHours());
    }

    public function test_undertime_hours(): void
    {
        $timeCalculator = new WorkTimeCalculator(
            Carbon::createFromTimeString('9:00am'),
            Carbon::createFromTimeString('11:00am'),
        );

        $this->assertEquals(CarbonInterval::hour(0), $timeCalculator->overtimeHours());
        $this->assertEquals(CarbonInterval::hour(6), $timeCalculator->undertimeHours());
    }
}
