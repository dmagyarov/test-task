<?php

namespace Tests\Unit;

use App\Actions\CreateUserAction;
use App\DTO\CreateUserDTO;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateUserActionTest extends TestCase
{
    use RefreshDatabase;

    protected CreateUserDTO $userDto;

    /**
     * @return void
     */
    public function test_that_user_is_created(): void
    {
        $userDto = new CreateUserDTO(
            fake()->safeEmail,
            fake()->password,
            fake()->firstName,
            fake()->firstName,
            fake()->lastName,
            fake()->e164PhoneNumber
        );

        $user = app(CreateUserAction::class)->handle($userDto);

        $this->assertEquals($userDto->email, $user->email);
        $this->assertEquals($userDto->firstName, $user->first_name);
        $this->assertEquals($userDto->middleName, $user->middle_name);
        $this->assertEquals($userDto->lastName, $user->last_name);
        $this->assertEquals($userDto->contactNo, $user->contact_no);

        $this->assertDatabaseHas('users', [
            'email' => $userDto->email,
            'first_name' => $userDto->firstName,
            'middle_name' => $userDto->middleName,
            'last_name' => $userDto->lastName,
            'contact_no' => $userDto->contactNo
        ]);
    }
}
