<?php

namespace Tests\Unit;

use App\DTO\CreateWorkLogDTO;
use App\Models\User;
use App\Services\WorkLogService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class WorkLogCreationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     */
    public function test_work_log_entry_is_created(): void
    {
        $user = User::factory()->count(1)->create()->first();

        $workLogService = app(WorkLogService::class);
        $workLogDTO = new CreateWorkLogDTO(
            $user->id,
            today(),
            Carbon::createFromTimeString('10:00 am'),
            Carbon::createFromTimeString('10:00 pm')
        );

        $workLogService->create($workLogDTO);

        $this->assertDatabaseHas('work_log', [
            'user_id' => $user->id,
            'date' => today()->format('Y-m-d'),
            'time_in' => '10:00:00',
            'time_out' => '22:00:00',
            'hours_worked' => '11:00:00',
            'hours_late' => '01:00:00',
            'hours_undertime' => '00:00:00',
            'hours_overtime' => '03:00:00'
        ]);
    }
}
