<?php

namespace Tests\Unit;

use App\DTO\WorkLogIndexDTO;
use App\Http\Requests\WorkLogIndexRequest;
use App\Models\User;
use App\Models\WorkLog;
use App\Repositories\WorkLogRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class WorkLogRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_work_log_index(): void
    {
        $users = User::factory()->count(100)->create();
        $workLogs = WorkLog::factory()->count(1000)->create();

        $repository = new WorkLogRepository;
        $request = new WorkLogIndexRequest;

        $result = $repository->all(new WorkLogIndexDTO('date', 'desc', 10));
        $this->checkRepositoryFiltering($result, 10);
        $this->checkRepositorySort($result, 'date');
    }

    /**
     * @param LengthAwarePaginator $result
     * @param string $key
     */
    private function checkRepositorySort(LengthAwarePaginator $result, string $key): void
    {
        $this->assertEquals(collect($result->items()), collect($result->items())->sortByDesc($key));
    }

    /**
     * @param LengthAwarePaginator $result
     * @return void
     */
    private function checkRepositoryFiltering(LengthAwarePaginator $result, $userId): void
    {
        $this->assertEquals(
            $userId, \Arr::get($result->items(), 1, $result->items()[0])->user_id
        );
    }
}
